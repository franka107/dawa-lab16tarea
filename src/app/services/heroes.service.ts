import { Injectable } from '@angular/core';
import { CompileShallowModuleMetadata } from '@angular/compiler';

@Injectable({ providedIn: 'root' })
export class HeroesService {
  private heroes: Heroe[] = [
    {
      nombre: 'Spiderman',
      bio: 'lorem ipsum color',
      img:
        'https://cronicaglobal.elespanol.com/uploads/s1/61/11/50/7/main-700b9bff30.jpeg',
      aparicion: '123-123-123',
      casa: 'dc',
    },
  ];

  constructor() {
    console.log('Servicio listo');
  }

  getHeroes(): Heroe[] {
    return this.heroes;
  }

  getHeroe(idx: string) {
    return this.heroes[idx];
  }
}

export interface Heroe {
  nombre: string;
  bio: string;
  img: string;
  aparicion: string;
  casa: string;
}
